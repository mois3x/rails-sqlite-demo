class HabitantesController < ApplicationController
  # GET /habitantes
  # GET /habitantes.json
  def index
    @habitantes = Habitante.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @habitantes }
    end
  end

  # GET /habitantes/1
  # GET /habitantes/1.json
  def show
    @habitante = Habitante.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @habitante }
    end
  end

  # GET /habitantes/new
  # GET /habitantes/new.json
  def new
    @habitante = Habitante.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @habitante }
    end
  end

  # GET /habitantes/1/edit
  def edit
    @habitante = Habitante.find(params[:id])
  end

  # POST /habitantes
  # POST /habitantes.json
  def create
    @habitante = Habitante.new(params[:habitante])

    respond_to do |format|
      if @habitante.save
        format.html { redirect_to @habitante, notice: 'Habitante was successfully created.' }
        format.json { render json: @habitante, status: :created, location: @habitante }
      else
        format.html { render action: "new" }
        format.json { render json: @habitante.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /habitantes/1
  # PUT /habitantes/1.json
  def update
    @habitante = Habitante.find(params[:id])

    respond_to do |format|
      if @habitante.update_attributes(params[:habitante])
        format.html { redirect_to @habitante, notice: 'Habitante was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @habitante.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /habitantes/1
  # DELETE /habitantes/1.json
  def destroy
    @habitante = Habitante.find(params[:id])
    @habitante.destroy

    respond_to do |format|
      format.html { redirect_to habitantes_url }
      format.json { head :no_content }
    end
  end
end
