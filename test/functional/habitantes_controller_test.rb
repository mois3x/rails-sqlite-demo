require 'test_helper'

class HabitantesControllerTest < ActionController::TestCase
  setup do
    @habitante = habitantes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:habitantes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create habitante" do
    assert_difference('Habitante.count') do
      post :create, habitante: { avenida: @habitante.avenida, calle: @habitante.calle, cedula: @habitante.cedula, manzana_id: @habitante.manzana_id, nombre: @habitante.nombre, telefono: @habitante.telefono }
    end

    assert_redirected_to habitante_path(assigns(:habitante))
  end

  test "should show habitante" do
    get :show, id: @habitante
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @habitante
    assert_response :success
  end

  test "should update habitante" do
    put :update, id: @habitante, habitante: { avenida: @habitante.avenida, calle: @habitante.calle, cedula: @habitante.cedula, manzana_id: @habitante.manzana_id, nombre: @habitante.nombre, telefono: @habitante.telefono }
    assert_redirected_to habitante_path(assigns(:habitante))
  end

  test "should destroy habitante" do
    assert_difference('Habitante.count', -1) do
      delete :destroy, id: @habitante
    end

    assert_redirected_to habitantes_path
  end
end
