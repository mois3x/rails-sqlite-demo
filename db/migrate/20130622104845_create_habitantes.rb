class CreateHabitantes < ActiveRecord::Migration
  def change
    create_table :habitantes do |t|
      t.integer :cedula
      t.string :nombre
      t.integer :manzana_id
      t.string :calle
      t.string :avenida
      t.string :telefono

      t.timestamps
    end
  end
end
